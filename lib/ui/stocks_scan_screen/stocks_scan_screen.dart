import 'dart:io';

import 'package:collection/collection.dart';
import 'package:excel/excel.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show SystemUiOverlayStyle;
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:scancode/data/db/db_helper.dart';
import 'package:scancode/ui/components/snack_bar_widget.dart';
import 'package:scancode/utils/logger.dart';
import 'package:share_plus/share_plus.dart';
import 'package:simple_barcode_scanner/simple_barcode_scanner.dart';
import 'package:pdf/widgets.dart' as pw;
import '../../data/model/stock_item_model.dart';


class StockScanScreen extends StatefulWidget {
  const StockScanScreen({Key? key}) : super(key: key);

  @override
  State<StockScanScreen> createState() => _StockScanScreenState();
}

class _StockScanScreenState extends State<StockScanScreen> {
  List<StockItem> _masterData = [];
  List<StockItem> _scannedData = [];
  String? filePath;
  DBHelper? dbHelper;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      Log.info("DB init done");
      dbHelper = DBHelper(dbInstance: await DataBase.initDb());
      loadMasterData();
      refreshScannedItemsList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          systemOverlayStyle: const SystemUiOverlayStyle(
            // Status bar color
            statusBarColor: Colors.white,
            // Status bar brightness (optional)
            statusBarIconBrightness: Brightness.dark, // For Android (dark icons)
            statusBarBrightness: Brightness.light, // For iOS (dark icons)
          ),
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: const Text("Scan Stock App",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.black,
          ),),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    child: const Text("Upload File"),
                    onPressed:(){
                      _pickFile();
                    },
                  ),
                  if (_masterData.isNotEmpty) ... {
                    ElevatedButton(
                      onPressed: () async {
                        openScanScreen();
                      },
                      onLongPress: () {
                        loadDummyScannedItems();
                      },
                      child: const Text('Scan Code'),
                    ),
                    if (_scannedData.isNotEmpty)
                      ElevatedButton(
                        child: const Text("Clear"),
                        onPressed:(){
                          removeScannedStocksList();
                        },
                      ),
                  }
                ],
              ),
            ),
            if (_scannedData.isNotEmpty) ... {
              Expanded(
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: DataTable(
                        columnSpacing: 30,
                        columns: [
                          // Set the name of the column
                          DataColumn(label: _headerText('Bar Code'),),
                          DataColumn(label: _headerText('Design No'),),
                          DataColumn(label: _headerText('Lot No'),),
                          DataColumn(label: _headerText('Color'),),
                        ],
                        rows:
                        _scannedData.map((stock) =>
                            DataRow(cells: [
                              DataCell(Text(stock.barcode.toString())),
                              DataCell(Text(stock.designNo.toString())),
                              DataCell(Text(stock.lotNo.toString())),
                              DataCell(Text(stock.color.toString())),
                            ])).toList()
                    ),
                  ),
                ),
              ),
            }
          ],
        ),
        floatingActionButton: _scannedData.isEmpty ? null : FloatingActionButton(
          onPressed: () async {
            if (_scannedData.isNotEmpty) {
              final pdf = pw.Document();
              pdf.addPage(
                pw.MultiPage(
                  build: (context) => [
                    pw.Center(child: pw.Text("Scanned items", style: const pw.TextStyle(fontSize: 20,))),
                    pw.SizedBox(height: 4),
                    pw.Center(child: pw.Text("Exported time: ${DateFormat('yyyy-MMM-dd hh:mm:a').format(DateTime.now())}")),
                    pw.SizedBox(height: 10),
                    pw.Table.fromTextArray(context: context, data: <List<String>>[
                      <String>['Barcode', 'Design No', 'Lot No', 'Color'],
                      ..._scannedData.map(
                              (item) => [item.barcode ?? '', item.designNo ?? '', item.lotNo ?? '', item.color ?? '']).toList()
                    ]),
                  ],
                ),
              );
              saveFile(pdf, 'scanned_items');
            } else {
              showSnackBar(context: context, message: "No results found");
            }
          },
          tooltip: 'Export PDF',
          child: const Icon(Icons.share_rounded),
        ),
    );

  }

  void _pickFile() async {

    try {
      final result = await FilePicker.platform.pickFiles(
          type: FileType.custom,
          withData: true,
          allowedExtensions: ['xlsx'],
          allowMultiple: false
      );

      // if no file is picked
      if (result == null) return;

      // Read the selected Excel file
      var bytes = result.files.first.bytes!;
      var excel = Excel.decodeBytes(bytes);

      // Get the first sheet in the Excel file
      var sheet = excel.tables.keys.first;
      var rows = excel.tables[sheet]?.rows;

      List<Map<String, dynamic>> jsonData = [];

      for (var i = 1; i < rows!.length; i++) {
        Map<String, dynamic> rowMap = {};
        for (var j = 0; j < rows[i].length; j++) {
          // Get the header value for this column
          var header = rows[0][j]!.value.toString();

          // Get the cell value for this column
          var cellValue = rows[i][j]!.value.toString();

          // Add the cell value to the row map using the header as the key
          rowMap[header] = cellValue;
        }
        jsonData.add(rowMap);
      }


      List<StockItem> list = [];
      for (var stockJson in jsonData) {
        list.add(StockItem.fromJson(stockJson));
      }

      // Add to DB
      dbHelper?.deleteAllStocks();
      List resList =[];
      for (int s =0; s < list.length; s++) {
        resList.add((await dbHelper?.insertStock(list[s])));
        if (resList.length == list.length) {
          showSnackBar(context: context, message: "Stocks uploaded successfully");
          loadMasterData();
        }
      }

      setState(() {
        // _scannedData = _scannedData;
      });
    } catch(e) {
      Log.error("File pick issue ${e.toString()}");
    }
  }

  Future<void> loadMasterData() async {
    _masterData = await dbHelper?.getAllStocks() ?? [];
  }

  Future<void> openScanScreen() async {
    var scanRes = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => const SimpleBarcodeScannerPage(),
        ));
    if (scanRes is String) {
      scanRes = scanRes.toString().trim();
      Log.info("Scanned response $scanRes");
      if (scanRes != "-1") {
        try {
          StockItem? item = _masterData.firstWhereOrNull((element) => element.id == scanRes.toString());
          if (item != null) {
            await dbHelper?.insertScannedItem(item);
            showStockDialog(item, callback: () {
              openScanScreen();
            });
            refreshScannedItemsList();
          } else {
            showSnackBar(context: context, message: "Results not found");
          }
        } catch (e) {
          Log.error("Scan match error ${e.toString()}");
          showSnackBar(context: context, message: "Something went wrong");
        }
      }
    }
  }

  void refreshScannedItemsList() async{
    _scannedData = await dbHelper?.getAllScannedItems() ?? [];
    setState(() {

    });
  }

  void showStockDialog(StockItem? stock, {Function? callback}) {
    Dialog dialog = Dialog(
      shape: RoundedRectangleBorder(
          borderRadius:BorderRadius.circular(30.0)),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SizedBox(
          height: 300,
          width: 300,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const FlutterLogo(size: 50,),
              Text("Scanned Info"
                  "\n${stock?.toString()}",
                textAlign: TextAlign.center,
                style:const TextStyle(fontSize: 20,),
              ),
              ElevatedButton(
                  onPressed: () {
                        Navigator.of(context).pop();
                        callback?.call();
                  }, child: const Text("Close"))
            ],
          ),
        ),
      ),
    );

    showDialog(context: context, builder: (BuildContext context) => dialog);
  }

  void removeScannedStocksList() {
    dbHelper?.deleteAllScannedItems();
    setState(() {
      _scannedData.clear();
    });
  }

  Future<void> saveFile(document, String name) async {
    final Directory dir = await getApplicationDocumentsDirectory();
    final File file = File('${dir.path}/$name.pdf');

    await file.writeAsBytes(await document.save());
    debugPrint('Saved exported PDF at: ${file.path}');
    shareFile(file.path);
  }

  Future<void> shareFile(String path) async {
    await Share.shareFiles(['$path'], text: 'Share');
    Log.info("Export done");
  }

  void loadDummyScannedItems() {
    /*setState(() {
      _scannedData.addAll(_masterData);
    });*/
  }

  Widget _headerText(String data) {
    return Text(
      data,
      style: const TextStyle(
        fontWeight: FontWeight.bold,
        color: Colors.black,
      ),
    );
  }

}
