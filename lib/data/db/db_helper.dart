

import 'dart:io';

import 'package:scancode/utils/logger.dart';
import 'package:sqflite/sqflite.dart';

import '../model/stock_item_model.dart';

class DataBase {

  static const String dbName = "stocks.db";
  static const String tblStocks = "stocks";
  static const String tblScannedItems = "scanned_items";

  static Future<Database> initDb() async {
    var databasesPath = await getDatabasesPath();
    String path = '$databasesPath/$dbName';
    //bool isMobile = Platform.isAndroid || Platform.isIOS;
    Database dbInstance = await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute('CREATE TABLE $tblStocks (id TEXT PRIMARY KEY, barcode TEXT,designno TEXT,lotno TEXT,color TEXT)');
        await db.execute('CREATE TABLE $tblScannedItems (id TEXT PRIMARY KEY, barcode TEXT,designno TEXT,lotno TEXT,color TEXT)');
      }
    );
    return dbInstance;
  }
}


class DBHelper {
  Database dbInstance;

  DBHelper({required this.dbInstance});

  Future<int> insertStock(StockItem stock) async {
    stock.id = "${stock.lotNo}${stock.color}";
    return await dbInstance.insert(
      DataBase.tblStocks,
      stock.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> deleteAllStocks() async {
    Log.error("Deleting all stocks");
    await dbInstance.delete(DataBase.tblStocks);
  }

  Future<void> deleteAllScannedItems() async {
    Log.error("Deleting all scanned items");
    await dbInstance.delete(DataBase.tblScannedItems);
  }

  Future<StockItem?> getStock(String id) async {
    List<Map<String, dynamic>> data = await dbInstance.query(DataBase.tblStocks, where: 'id = ?', whereArgs: [id]);
    List<StockItem> book = listMapToStockItem(data);
    if (book.isNotEmpty) {
      return book.first;
    }
    return null;
  }

  Future<List<StockItem>> getAllStocks() async {
    final List<Map<String, dynamic>> maps = await dbInstance.query(DataBase.tblStocks);
    return listMapToStockItem(maps);
  }

  Future<List<StockItem>> getAllScannedItems() async {
    final List<Map<String, dynamic>> maps = await dbInstance.query(DataBase.tblScannedItems);
    return listMapToStockItem(maps);
  }

  Future<int> insertScannedItem(StockItem stock) async {
    stock.id = stock.id ?? "${stock.lotNo}${stock.color}";
    return await dbInstance.insert(
      DataBase.tblScannedItems,
      stock.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  List<StockItem> listMapToStockItem(List<Map<String, dynamic>> maps) {
    List<StockItem> list = List.generate(maps.length, (i) {
      return StockItem.fromJson(maps[i]);
    });
    return list.reversed.toList();
  }
}