import 'dart:convert';

StockItem stockItemFromJson(String str) => StockItem.fromJson(json.decode(str));

String stockItemToJson(StockItem data) => json.encode(data.toJson());

class StockItem {
  String? id;
  String? barcode;
  String? designNo;
  String? lotNo;
  String? color;

  StockItem({
    this.id,
    this.barcode,
    this.designNo,
    this.lotNo,
    this.color,
  });

  factory StockItem.fromJson(Map<String, dynamic> json) => StockItem(
    id: json["id"],
    barcode: json["barcode"],
    designNo: json["designno"],
    lotNo: json["lotno"],
    color: json["color"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "barcode": barcode,
    "designno": designNo,
    "lotno": lotNo,
    "color": color,
  };

  @override
  String toString() {
    return 'id: $id \nBarcode: $barcode \nDesignNo: $designNo \nLotNo: $lotNo \ncolor: $color';
  }
}